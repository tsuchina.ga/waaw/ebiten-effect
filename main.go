package main

import (
	"log"
	"gitlab.com/tsuchina.ga/waaw/ebiten-effect/core"
	"time"
	"gitlab.com/tsuchina.ga/waaw/ebiten-effect/scene"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// locationの設定
	const location = "Asia/Tokyo"
	loc, err := time.LoadLocation(location)
	if err != nil {
		loc = time.FixedZone(location, 9*60*60)
	}
	time.Local = loc
}

func main() {

	c := core.DefaultSetting(&scene.FirstScene{
		ScreenW: 400,
		ScreenH: 300,
	})
	c.IsDebug = true
	c.IsRunnableBackground = true
	c.Run()
}
