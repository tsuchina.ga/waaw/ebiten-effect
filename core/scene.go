package core

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"fmt"
	"log"
)

type Scene interface {
	Update(*Input) error
	Draw(*ebiten.Image) error
	Next() Scene
}

const (
	SceneStatusIn   = 0
	SceneStatusKeep = 1
	SceneStatusOut  = 2
)

type SceneBase struct {
	Title  string
	Effect Effect
	Status int
}

func (s *SceneBase) Update(*Input) error {
	if s.Effect == nil {
		s.Effect = new(EffectBase)
	}

	return nil
}

func (s *SceneBase) Draw(r *ebiten.Image) error {
	switch s.Status {
	case SceneStatusIn:
		return s.Effect.In(r, s)
	case SceneStatusOut:
		return s.Effect.Out(r, s)
	}
	return nil
}

func (s *SceneBase) Next() Scene {
	return nil
}

type SceneManager struct {
	image        *ebiten.Image
	current      Scene
	next         Scene
	prev         Scene
	isDebug      bool
	currentImage *ebiten.Image
	prevImage    *ebiten.Image
}

func (m *SceneManager) update(input *Input) (err error) {

	if m.isDebug == true {
		ebitenutil.DebugPrint(m.image, fmt.Sprintf("fps: %f, tps: %f", ebiten.CurrentFPS(), ebiten.CurrentTPS()))
		ebitenutil.DebugPrint(m.image, fmt.Sprintf("\ninput: %+v", input.buttons))
	}

	if m.next != nil {
		m.prev = m.current
		m.current = m.next
		m.next = nil
		log.Println("シーンが変わりました")
	}

	if err = m.current.Update(input); err != nil {
		return
	}

	if err = m.prev.Draw(m.image); err != nil {
		return
	}

	if err = m.current.Draw(m.image); err != nil {
		return
	}

	if next := m.current.Next(); next != nil {
		m.next = next
	}

	return
}
