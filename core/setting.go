package core

import (
	"github.com/hajimehoshi/ebiten"
	"log"
)

type Setting struct {
	ScreenWidth          int
	ScreenHeight         int
	Scale                float64
	Title                string
	FirstScene           Scene
	MaxTps               int
	IsRunnableBackground bool
	IsDebug              bool
	sceneManager         *SceneManager
	input                *Input
}

func (s *Setting) Run() (err error) {
	if s.FirstScene == nil {
		log.Fatalln("FirstSceneが設定されていません")
	}

	if s.MaxTps > 0 {
		ebiten.SetMaxTPS(s.MaxTps)
	}
	ebiten.SetRunnableInBackground(s.IsRunnableBackground)

	if err := ebiten.Run(s.update, s.ScreenWidth, s.ScreenHeight, s.Scale, s.Title); err != nil {
		log.Fatalln(err)
	}
	return
}

func (s *Setting) update(r *ebiten.Image) (err error) {

	if s.sceneManager == nil {
		current, _ := ebiten.NewImage(s.ScreenWidth, s.ScreenHeight, ebiten.FilterDefault)
		prev, _ := ebiten.NewImage(s.ScreenWidth, s.ScreenHeight, ebiten.FilterDefault)

		s.sceneManager = &SceneManager{
			image:   r,
			current: s.FirstScene,
			isDebug: s.IsDebug,
			currentImage: current,
			prevImage: prev,
		}
	}

	if s.input == nil {
		s.input = new(Input)
	}

	s.input.update(r)

	err = s.sceneManager.update(s.input)
	return
}

func DefaultSetting(firstScene Scene) *Setting {
	return &Setting{
		ScreenWidth:  400,
		ScreenHeight: 300,
		Scale:        2,
		Title:        "My Game Title",
		FirstScene:   firstScene,
		MaxTps:       30,
	}
}
