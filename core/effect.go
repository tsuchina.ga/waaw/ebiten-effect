package core

import "github.com/hajimehoshi/ebiten"

type Effect interface {
	In(*ebiten.Image, Scene) error
	Out(*ebiten.Image, Scene) error
	IsFinishedIn() bool
	IsFinishedOut() bool
}

type EffectBase struct {
	IsInEffect         bool
	IsOutEffect        bool
	InTransitionFrame  int
	InTransitionCount  int
	OutTransitionFrame int
	OutTransitionCount int
}

func (e *EffectBase) In(r *ebiten.Image, s Scene) (err error) {
	if e.InTransitionFrame-e.InTransitionCount >= 0 {
		e.InTransitionCount++
		if e.IsInEffect {
			err = s.Draw(r)
		}
	}
	return
}

func (e *EffectBase) Out(r *ebiten.Image, s Scene) (err error) {
	if e.OutTransitionFrame-e.OutTransitionCount >= 0 {
		e.OutTransitionCount++
		if e.IsOutEffect {
			err = s.Draw(r)
		}
	}
	return
}

func (e *EffectBase) IsFinishedIn() bool {
	return e.InTransitionFrame - e.InTransitionCount == 0
}

func (e *EffectBase) IsFinishedOut() bool {
	return e.OutTransitionFrame - e.OutTransitionCount == 0
}
