package scene

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"gitlab.com/tsuchina.ga/waaw/ebiten-effect/core"
)

type FirstScene struct {
	core.SceneBase
	ScreenW   int
	ScreenH   int
	count     int
	direction int
	square    *ebiten.Image
	w         int
	h         int
	x         float64
	y         float64
	next      core.Scene
}

func (s *FirstScene) Update(input *core.Input) (err error) {
	if s.w < 1 && s.h < 1 {
		s.w = 30
		s.h = 30
	}

	s.count++
	if s.count > 30 {
		s.count = 1
		s.direction++
		if s.direction == 4 {
			s.direction = 0
		}
	}

	switch s.direction {
	case 0:
		s.x += float64(s.ScreenW-30) / 30
	case 1:
		s.y += float64(s.ScreenH-30) / 30
	case 2:
		s.x -= float64(s.ScreenW-30) / 30
	case 3:
		s.y -= float64(s.ScreenH-30) / 30
	}

	if input.GetKey(ebiten.KeySpace).IsPressing {
		s.next = &SecondScene{
			ScreenW:   s.ScreenW,
			ScreenH:   s.ScreenH,
			x:         s.x,
			y:         s.y,
			w:         s.w,
			h:         s.h,
			direction: s.direction,
			count:     30 - s.count,
		}
	}

	return
}

func (s *FirstScene) Draw(r *ebiten.Image) (err error) {
	if s.square == nil {
		s.square, err = ebiten.NewImage(s.w, s.h, ebiten.FilterNearest)
		if err != nil {
			return
		}

		s.square.Fill(color.White)
	}

	opts := &ebiten.DrawImageOptions{}
	opts.GeoM.Translate(s.x, s.y)
	r.DrawImage(s.square, opts)
	return
}

func (s *FirstScene) Next() (n core.Scene) {
	return s.next
}
