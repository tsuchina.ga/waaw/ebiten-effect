package scene

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"gitlab.com/tsuchina.ga/waaw/ebiten-effect/core"
)

type SecondScene struct {
	core.SceneBase
	ScreenW   int
	ScreenH   int
	count     int
	direction int
	square    *ebiten.Image
	w         int
	h         int
	x         float64
	y         float64
	next      core.Scene
}

func (s *SecondScene) Draw(r *ebiten.Image) (err error) {
	if s.square == nil {
		s.square, err = ebiten.NewImage(s.w, s.h, ebiten.FilterNearest)
		if err != nil {
			return
		}

		s.square.Fill(color.White)
	}

	opts := &ebiten.DrawImageOptions{}
	opts.GeoM.Translate(s.x, s.y)
	r.DrawImage(s.square, opts)
	return
}
