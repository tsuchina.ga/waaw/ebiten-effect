package core

import (
	"github.com/hajimehoshi/ebiten/inpututil"
	"github.com/hajimehoshi/ebiten"
	"time"
)

type Input struct {
	buttons map[ebiten.Key]*Button
}

func (i *Input) update(r *ebiten.Image) {
	if i.buttons == nil {
		i.buttons = make(map[ebiten.Key]*Button)
	}

	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		i.buttons[ebiten.KeySpace] = &Button{IsPressing: true, KeyName: "KeySpace", PressedAt: time.Now().UnixNano()}
	}
	if inpututil.IsKeyJustReleased(ebiten.KeySpace) {
		i.buttons[ebiten.KeySpace].ReleasedAt = time.Now().UnixNano()
		i.buttons[ebiten.KeySpace].IsPressing = false
	}
}

func (i *Input) GetKey(k ebiten.Key) *Button {
	if _, ok := i.buttons[k]; !ok {
		i.buttons[k] = new(Button)
	}
	return i.buttons[k]
}

type Button struct {
	IsPressing bool
	KeyName    string
	PressedAt  int64
	ReleasedAt int64
}
